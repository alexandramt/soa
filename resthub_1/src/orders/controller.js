
Order = require('./model');

exports.index = function (req, res) {
    Order.get(function (err, orders) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Orders retrieved successfully",
            data: orders
        });
    });
};

exports.new = function (req, res) {
    var order = new Order();
    order.address = req.body.address;
    order.time = req.body.time;
    order.date = req.body.date ? req.body.date : order.date;
    order.amount = req.body.amount;
    order.dishes = req.body.dishes;

// save the contact and check for errors
    order.save(function (err) {
        if (err)
            res.json(err);
        else {
            res.json({
                message: 'New order created!',
                data: order
            });
        }
    });
};

exports.view = function (req, res) {
    Order.findById(req.params.order_id, function (err, order) {
        if (err)
            res.send(err);
        res.json({
            message: 'Order details loading..',
            data: order
        });
    });
};

exports.update = function (req, res) {
    Order.findById(req.params.order_id, function (err, order) {
        if (err)
            res.send(err);
        order.address = req.body.address;
        order.time = req.body.time;
        order.date = req.body.date ? req.body.date : order.date;
        order.amount = req.body.amount;
        order.dishes = req.body.dishes;
// save the contact and check for errors
        order.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Order Info updated',
                data: order
            });
        });
    });
};

exports.delete = function (req, res) {
    Order.remove({
        _id: req.params.order_id
    }, function (err, order) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Order cancelled'
        });
    });
};