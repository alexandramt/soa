// contactModel.js
var mongoose = require('mongoose');
// Setup schema
var orderSchema = mongoose.Schema({
    dishes : [],
    address: String,
    date: {
        type: Date,
        default: Date.now()
    },
    time: String,
    amount: Number
});
//
// var contactSchema = mongoose.Schema({
//
//     name: {
//         type: String,
//         required: true
//     },
//     email: {
//         type: String,
//         required: true
//     },
//     gender: String,
//     phone: String,
//     create_date: {
//         type: Date,
//         default: Date.now
//     }
// });
// Export Contact model
// var Contact = module.exports = mongoose.model('contact', contactSchema);
var Order = module.exports = mongoose.model('order', orderSchema);

module.exports.get = function (callback, limit) {
    Order.find(callback).limit(limit);
}
// module.exports.get = function (callback, limit) {
//     Contact.find(callback).limit(limit);
// }
