let express = require('express');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let app = express();

let apiRoutes = require("./dishes/routes");

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost/soa_project');
var db = mongoose.connection;


var port = process.env.PORT || 8081;

app.get('/', (req, res) => res.send('Hello World with Express from resthub 2'));

app.use('/api', apiRoutes);

app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});

