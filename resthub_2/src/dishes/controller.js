Dish = require('./model');
// Handle index actions
exports.index = function (req, res) {
    Dish.get(function (err, dishes) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Dishes retrieved successfully",
            data: dishes
        });
    });
};
// Handle create contact actions
exports.new = function (req, res) {
    var dish = new Dish();
    dish.name = req.body.name;
    dish.imageUrl = req.body.imageUrl;
    dish.price = req.body.price;
    dish.portions = req.body.portions;
// save the contact and check for errors
    dish.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: 'New dish created!',
            data: dish
        });
    });
};
exports.view = function (req, res) {
    Dish.findById(req.params.dish_id, function (err, dish) {
        if (err)
            res.send(err);
        res.json({
            message: 'Dish details loading..',
            data: dish
        });
    });
};
// Handle update contact info
exports.update = function (req, res) {
    Dish.findById(req.params.dish_id, function (err, dish) {
        if (err)
            res.send(err);
        dish.name = req.body.name;
        dish.imageUrl = req.body.imageUrl;
        dish.price = req.body.price;
        dish.portions = req.body.portions;
// save the contact and check for errors
        dish.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Dish Info updated',
                data: dish
            });
        });
    });
};
// Handle delete contact
exports.delete = function (req, res) {
    Dish.remove({
        _id: req.params.dish_id
    }, function (err, dish) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Dish deleted'
        });
    });
};