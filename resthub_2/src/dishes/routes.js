let router = require('express').Router();
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub 2 crafted with love!',
    });
});

var dishController = require('./controller');

router.route('/dishes')
    .get(dishController.index)
    .post(dishController.new);
router.route('/dishes/:dish_id')
    .get(dishController.view)
    .patch(dishController.update)
    .put(dishController.update)
    .delete(dishController.delete);

module.exports = router;
